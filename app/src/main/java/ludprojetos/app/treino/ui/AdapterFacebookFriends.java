package ludprojetos.app.treino.ui;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;

import ludprojetos.app.treino.R;
import ludprojetos.app.treino.facebook.Facebook;
import ludprojetos.app.treino.facebook.model.FacebookFriend;

public class AdapterFacebookFriends extends BaseAdapter {
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<FacebookFriend> facebookFriends = new ArrayList<>();

    private Facebook facebook;

    public AdapterFacebookFriends(Context context, ArrayList<FacebookFriend> facebookFriends) {
        this.context = context;
        this.facebookFriends = facebookFriends;

        inflater = (LayoutInflater.from(context));

        facebook = new Facebook();
    }

    @Override
    public int getCount() {
        return facebookFriends.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.friend_list_item, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        facebook.getImageProfile(context, facebookFriends.get(position).getId(), viewHolder.avatar);

        viewHolder.name.setText(facebookFriends.get(position).getName());

        return convertView;
    }

    private class ViewHolder{
        TextView name;
        ImageView avatar;

        public ViewHolder(View convertView) {
            name = (TextView) convertView.findViewById(R.id.textview_name);
            avatar = (ImageView) convertView.findViewById(R.id.imageview_avatar);
        }
    }
}
