package ludprojetos.app.treino.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import ludprojetos.app.treino.R;

public class ToDoListFragment extends Fragment{
    private Context context;
    private View fragmentView;

    private FloatingActionButton fab;
    private ListView listView;

    private AdapterToDo adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        fragmentView = inflater.inflate(R.layout.fragment_task_list, container, false);

        fab = (FloatingActionButton) fragmentView.findViewById(R.id.fab);
        listView = (ListView) fragmentView.findViewById(R.id.listViewTask);

        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(context, ToDoInsertActivity.class));
            }
        });

        adapter = new AdapterToDo(context);
        listView.setAdapter(adapter);

        if (container != null) {
            container.removeAllViews();
        }
        return fragmentView;
    }
}
