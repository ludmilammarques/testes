package ludprojetos.app.treino.ui;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.Profile;
import com.facebook.login.widget.LoginButton;

import java.util.ArrayList;

import ludprojetos.app.treino.R;
import ludprojetos.app.treino.facebook.Facebook;
import ludprojetos.app.treino.facebook.model.FacebookFriend;

public class LoginFragment extends Fragment{
    private Context context;
    private View fragmentView;

    private LoginButton loginButton;
    private TextView textView;
    private ImageView imageView;
    private ListView listView;
    private Button buttonLogin;

    Facebook facebook;
    CallbackManager callbackManager;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        fragmentView = inflater.inflate(R.layout.fragment_login, container, false);

        setViews();
        facebook = new Facebook(this, loginButton);
        setContentViews();

        if (container != null) {
            container.removeAllViews();
        }
        return fragmentView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        facebook.getCallbackManager().onActivityResult(requestCode, resultCode, data);
    }

    private void setViews(){
        textView = (TextView) fragmentView.findViewById(R.id.dados);
        imageView = (ImageView) fragmentView.findViewById(R.id.perfil);
        listView = (ListView) fragmentView.findViewById(R.id.listView);
        loginButton = (LoginButton) fragmentView.findViewById(R.id.login_button);
        buttonLogin = (Button) fragmentView.findViewById(R.id.button_login);

        buttonLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(AccessToken.getCurrentAccessToken() == null)
                    facebook.signIn(getActivity());
                else {
                    buttonLogin.setText("Sair");
                    facebook.signOut();
                }
            }
        });
    }

    private void setContentViews(){
        if(Profile.getCurrentProfile() != null) {
            facebook.getImageProfile(context, Profile.getCurrentProfile().getId(), imageView);

            String mensagem = "Olá, " + Profile.getCurrentProfile().getName();
            textView.setText(mensagem);

            facebook.getFriends(new Facebook.FriendsCallback() {
                @Override
                public void onSuccess(ArrayList<FacebookFriend> result) {
                    AdapterFacebookFriends adapter = new AdapterFacebookFriends(context, result);
                    listView.setAdapter(adapter);
                }
            });
        }
    }
}
