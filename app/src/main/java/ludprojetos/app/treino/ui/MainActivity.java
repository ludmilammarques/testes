package ludprojetos.app.treino.ui;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.drawee.backends.pipeline.Fresco;
import com.sw926.imagefileselector.ImageFileSelector;

import ludprojetos.app.treino.R;
import ludprojetos.app.treino.imagePicker.ImagePicker;

public class MainActivity extends AppCompatActivity{

    private RelativeLayout toolbarBottom;
    private LinearLayout linearLayout1;
    private LinearLayout linearLayout2;
    private LinearLayout linearLayout3;
    private LinearLayout linearLayout4;

    private SimpleTarget target;
    private ImageFileSelector mImageFileSelector;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());

        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(getResources().getDrawable(R.drawable.ic_drawer));

        mImageFileSelector = new ImageFileSelector(MainActivity.this);

        Fresco.initialize(this);

        getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, new LoginFragment()).commit();

        setViews();
        setListeners();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode == RESULT_OK){
            //Handle da Lib
            mImageFileSelector.onActivityResult(requestCode, resultCode, data);

            //Handle da classe ImagePicker
            if(requestCode == ImagePicker.SELECT_FILE
                    || requestCode == ImagePicker.REQUEST_CAMERA) {
                if (data.getData() != null) {
                    Glide.with(this)
                            .load(data.getData())
                            .asBitmap()
                            .centerCrop()
                            .into(target);
                } else if (data.getExtras().get("data") != null) {
                    Glide.with(this)
                            .load(ImagePicker.getPathFromBitmap(data))
                            .asBitmap()
                            .centerCrop()
                            .into(target);
                }
            }
        }
    }

    private void setViews(){
        //Target do Glide que possibilita a manipulação do bitmap
        target = new SimpleTarget<Bitmap>(400, 400){
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {

            }
        };

        toolbarBottom = (RelativeLayout) findViewById(R.id.toolbarBottom);
        linearLayout1 = (LinearLayout) toolbarBottom.findViewById(R.id.button1);
        linearLayout2 = (LinearLayout) toolbarBottom.findViewById(R.id.button2);
        linearLayout3 = (LinearLayout) toolbarBottom.findViewById(R.id.button3);
        linearLayout4 = (LinearLayout) toolbarBottom.findViewById(R.id.button4);
    }

    private void setListeners(){
        linearLayout1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, new GlideFragment()).commit();
            }
        });

        linearLayout2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, new FrescoFragment()).commit();
            }
        });

        linearLayout3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, new ToDoListFragment()).commit();
            }
        });

        linearLayout4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getSupportFragmentManager().beginTransaction().replace(R.id.frame_content, new AddImageFragment()).commit();
            }
        });
    }
}
