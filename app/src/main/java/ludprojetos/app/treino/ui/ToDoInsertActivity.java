package ludprojetos.app.treino.ui;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import ludprojetos.app.treino.R;
import ludprojetos.app.treino.database.DbManager;

public class ToDoInsertActivity extends AppCompatActivity{

    private Button button;
    private EditText dateEditText;
    private EditText descriptionEditText;

    private DbManager dbManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task_insert);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        dbManager = new DbManager(this);

        button = (Button) findViewById(R.id.btn_salvar);
        dateEditText = (EditText) findViewById(R.id.date);
        descriptionEditText = (EditText) findViewById(R.id.description);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dbManager.insertTask(dateEditText.getText().toString(), descriptionEditText.getText().toString());
                Toast.makeText(ToDoInsertActivity.this, "Salvo com sucesso", Toast.LENGTH_SHORT).show();
                dateEditText.setText("");
                descriptionEditText.setText("");
            }
        });
    }
}
