package ludprojetos.app.treino.ui;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.sw926.imagefileselector.ImageFileSelector;

import ludprojetos.app.treino.R;
import ludprojetos.app.treino.imagePicker.ImagePicker;

public class AddImageFragment extends Fragment {
    private Context context;
    private View fragmentView;

    private Button buttonPickPhoto;
    private Button buttonPickPhotoWithLib;

    private ImageView imageView;

    private SimpleTarget target;
    private ImageFileSelector mImageFileSelector;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        fragmentView = inflater.inflate(R.layout.fragment_add_photos, container, false);

        mImageFileSelector = new ImageFileSelector(context);

        setViews();
        setListeners();

        if (container != null) {
            container.removeAllViews();
        }
        return fragmentView;
    }

    private void setViews(){
        buttonPickPhoto = (Button) fragmentView.findViewById(R.id.buttonWithOutLib);
        buttonPickPhotoWithLib = (Button) fragmentView.findViewById(R.id.buttonWithLib);

        imageView = (ImageView) fragmentView.findViewById(R.id.imageView);

        //Target do Glide que possibilita a manipulação do bitmap
        target = new SimpleTarget<Bitmap>(400, 400){
            @Override
            public void onResourceReady(Bitmap resource, GlideAnimation<? super Bitmap> glideAnimation) {
                imageView.setImageBitmap(resource);
            }
        };

    }

    private void setListeners(){
        buttonPickPhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ImagePicker.getChooserDialog(getActivity(),
                        "Tirar Foto",
                        "Pegar da Galeria",
                        "Cancelar",
                        "Adicionar Foto");
            }
        });

        //Listener com a Lib
        buttonPickPhotoWithLib.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final CharSequence[] namesButton = {"Tirar Foto", "Pegar da Galeria"};

                AlertDialog.Builder alertBuilder = new AlertDialog.Builder(context);
                alertBuilder.setTitle("Adicionar Foto");
                alertBuilder.setItems(namesButton, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (namesButton[which].equals("Tirar Foto")) {
                            mImageFileSelector.takePhoto(getActivity());
                        } else if (namesButton[which].equals("Pegar da Galeria")) {
                            mImageFileSelector.selectImage(getActivity());
                        }

                        mImageFileSelector.setCallback(new ImageFileSelector.Callback() {
                            @Override
                            public void onSuccess(final String file) {
                                Glide.with(context)
                                        .load(file)
                                        .asBitmap()
                                        .centerCrop()
                                        .into(target);
                            }

                            @Override
                            public void onError() {

                            }
                        });
                    }
                });
                alertBuilder.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });
                alertBuilder.show();
            }
        });

    }
}
