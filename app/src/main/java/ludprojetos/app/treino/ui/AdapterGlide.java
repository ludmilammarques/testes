package ludprojetos.app.treino.ui;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;

import ludprojetos.app.treino.R;

public class AdapterGlide extends BaseAdapter{
    private final Context context;
    private LayoutInflater inflater;
    private String vetorLinks[] = {
            "https://i.imgur.com/4B16IUF.jpg",
            "https://i.imgur.com/BISnx8Q.jpg",
            "https://i.imgur.com/87pr8b8.jpg",
            "https://i.imgur.com/24BLSkj.jpg",
            "https://i.imgur.com/eSb0z2K.jpg",
            "https://i.imgur.com/SEb1kTE.jpg",
            "https://i.imgur.com/K95pu3p.jpg",
            "https://i.imgur.com/VbnJ4OS.jpg",
            "https://i.imgur.com/V9uILjX.jpg",
            "https://i.imgur.com/05rfjzZ.jpg",
            "https://i.imgur.com/tnbA6VE.jpg",
            "https://i.imgur.com/cqFeH0L.jpg",
            "https://i.imgur.com/JlzpFxs.jpg",
            "https://i.imgur.com/NLFH5YN.jpg",
            "https://i.imgur.com/OdPeyPA.jpg",
            "https://i.imgur.com/viRCZCo.jpg",
            "https://i.imgur.com/rxxgjIJ.jpg",
            "https://i.imgur.com/7MSgoz8.jpg",
            "https://i.imgur.com/576xP1p.jpg",
            "https://i.imgur.com/WQz1KyA.jpg",
            "https://i.imgur.com/y5kHDA5.jpg",
            "https://i.imgur.com/5RYteFL.jpg"};

    public AdapterGlide(Context context) {
        this.context = context;
        inflater = (LayoutInflater.from(context));
    }

    @Override
    public int getCount() {
        return vetorLinks.length;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ViewHolder viewHolder;
        if(convertView == null){
            convertView = inflater.inflate(R.layout.photos_glide, null);
            viewHolder = new ViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        Glide.with(context)
                .load(vetorLinks[position])
                .centerCrop()
                .into(viewHolder.imageView);

        viewHolder.linearLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, PhotoFullActivity.class);
                intent.putExtra("URL", vetorLinks[position]);
                context.startActivity(intent);
            }
        });

        return convertView;
    }

    private class ViewHolder{
        LinearLayout linearLayout;
        ImageView imageView;
        public ViewHolder(View convertView) {
            this.linearLayout = (LinearLayout) convertView.findViewById(R.id.linearLayout);
            this.imageView = (ImageView) convertView.findViewById(R.id.imageView);
        }
    }
}
