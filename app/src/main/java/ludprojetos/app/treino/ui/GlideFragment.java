package ludprojetos.app.treino.ui;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import ludprojetos.app.treino.R;

public class GlideFragment extends Fragment {

    private Context context;
    private View fragmentView;

    private ListView listView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        context = getActivity();
        fragmentView = inflater.inflate(R.layout.fragment_glide, container, false);

        listView = (ListView) fragmentView.findViewById(R.id.listView);
        AdapterGlide adapter = new AdapterGlide(context);
        listView.setAdapter(adapter);

        if (container != null) {
            container.removeAllViews();
        }
        return fragmentView;
    }
}
