package ludprojetos.app.treino.facebook.model;

public class FacebookFriend {
    private String id;
    private String name;

    public FacebookFriend() {
    }

    public FacebookFriend(String name, String id) {
        this.name = name;
        this.id = id;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
