package ludprojetos.app.treino.facebook;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.Profile;
import com.facebook.internal.ImageRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

import ludprojetos.app.treino.facebook.model.FacebookFriend;

public class Facebook {
    private final String TAG = "FACEBOOK";
    private final String PUBLIC_PROFILE_KEY = "public_profile";
    private final String EMAIL_KEY = "email";
    private final String USER_FRIENDS_KEY = "user_friends";

    private CallbackManager callbackManager;

    public Facebook(Fragment fragment, LoginButton loginButton) {
        callbackManager = CallbackManager.Factory.create();
        if(fragment != null && loginButton != null) {
            loginButton.setReadPermissions(Arrays.asList(PUBLIC_PROFILE_KEY, EMAIL_KEY, USER_FRIENDS_KEY));
            loginButton.setFragment(fragment);
            registerCallback(loginButton);
        }
    }

    public Facebook() {
        callbackManager = CallbackManager.Factory.create();
        registerCallback();
    }

    private void registerCallback(LoginButton loginButton){
        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                //TODO integrar com o serviço
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    private void registerCallback(){
        LoginManager.getInstance().registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.e(TAG, "onSuccess: " + loginResult.getAccessToken() );
                GraphRequest request = GraphRequest.newMeRequest(
                        loginResult.getAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
                            @Override
                            public void onCompleted(JSONObject user, GraphResponse response) {
                                if (response.getError() != null) {
                                    //TODO
                                } else {
                                    //TODO
                                }
                            }
                        });
                Bundle parameters = new Bundle();
                parameters.putString("fields", EMAIL_KEY);
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.e(TAG, "onCancel");
            }

            @Override
            public void onError(FacebookException error) {
                error.printStackTrace();
            }
        });

    }

    public void signIn(Activity activity){
        LoginManager.getInstance().logInWithReadPermissions(activity, Arrays.asList(PUBLIC_PROFILE_KEY, EMAIL_KEY, USER_FRIENDS_KEY));
    }

    public void signOut(){
        LoginManager.getInstance().logOut();
    }

    public void getImageProfile(Context context, String profileId, ImageView imageView){
        if(AccessToken.getCurrentAccessToken() != null) {
            int dimensionPixelSize = context.getResources().getDimensionPixelSize(com.facebook.R.dimen.com_facebook_profilepictureview_preset_size_large);
            Uri profilePictureUri = ImageRequest.getProfilePictureUri(profileId, dimensionPixelSize, dimensionPixelSize);
            Glide.with(context)
                    .load(profilePictureUri)
                    .asBitmap()
                    .into(imageView);
        }
    }

    public void getFriends(final FriendsCallback friendsCallback){
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                "/" + Profile.getCurrentProfile().getId() +"/friends",
                null,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        try {
                            Gson gson = new Gson();
                            ArrayList<FacebookFriend> facebookFriends = gson.fromJson(response.getJSONObject().get("data").toString(),
                                    new TypeToken<ArrayList<FacebookFriend>>(){}.getType());

                            friendsCallback.onSuccess(facebookFriends);

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        ).executeAsync();
    }

    public CallbackManager getCallbackManager() {
        return callbackManager;
    }

    public interface FriendsCallback{
        void onSuccess(ArrayList<FacebookFriend> result);
    }
}
