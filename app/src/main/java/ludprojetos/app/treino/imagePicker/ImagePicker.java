package ludprojetos.app.treino.imagePicker;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class ImagePicker {
    public static final int REQUEST_CAMERA = 000;
    public static final int SELECT_FILE = 001;

    public static void getChooserDialog(final Activity activity, final String takePhotoButtonName, final String fromGalleryButtonName, String cancelButtonName, String dialogTitle){
        final CharSequence[] namesButton = {takePhotoButtonName, fromGalleryButtonName};

        AlertDialog.Builder alertBuilder = new AlertDialog.Builder(activity);
        alertBuilder.setTitle(dialogTitle);
        alertBuilder.setItems(namesButton, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                if(grantPermissions(activity)) {
                    if (namesButton[which].equals(takePhotoButtonName)) {
                        startCameraIntent(activity);
                    } else if (namesButton[which].equals(fromGalleryButtonName)) {
                        startGalleryIntent(activity);
                    }
                } else {
                    dialog.dismiss();
                }

            }
        });
        alertBuilder.setNegativeButton(cancelButtonName, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        alertBuilder.show();
    }

    private static boolean grantPermissions(Activity activity){
        final int GRANT_PERMISSION = 1;
        boolean grantPermission = true;
        if (ContextCompat.checkSelfPermission(activity,
                Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
            grantPermission = false;
            if (ActivityCompat.shouldShowRequestPermissionRationale(activity,
                    Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            } else {
                ActivityCompat.requestPermissions(activity,
                        new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                        GRANT_PERMISSION);
            }
        }

        return grantPermission;
    }

    private static void startCameraIntent(Activity activity){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        activity.startActivityForResult(intent, REQUEST_CAMERA);
    }

    private static void startGalleryIntent(Activity activity){
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        activity.startActivityForResult(Intent.createChooser(intent, ""), SELECT_FILE);
    }

    public static File getPathFromBitmap(Intent data){
        Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        thumbnail.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        File file = new File(Environment.getExternalStorageDirectory(),
                System.currentTimeMillis() + ".jpg");

        FileOutputStream fo;
        try {
            file.createNewFile();
            fo = new FileOutputStream(file);
            fo.write(bytes.toByteArray());
            fo.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return file;
    }

}
