package ludprojetos.app.treino.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;

import java.util.ArrayList;

import ludprojetos.app.treino.database.model.Task;
import ludprojetos.app.treino.database.tables.TaskTable;

public class DbManager {

    private static ContentResolver resolver;

    public DbManager(Context context) {
        resolver = context.getContentResolver();
    }

    public Uri insertTask(String date, String description){
        ContentValues values = new ContentValues(2);
        values.put(TaskTable.DATE, date);
        values.put(TaskTable.DESCRIPTION, description);

        return resolver.insert(Provider.TASK_URI, values);
    }

    public void deleteAllTask(){
        resolver.delete(Provider.TASK_URI, null, null);
    }

    public ArrayList<Task> getTasks(){
        Cursor cursor = resolver.query(Provider.TASK_URI, null, null, null, null);
        ArrayList<Task> arrayList = new ArrayList<>();

        try {
            if (cursor != null){
                cursor.moveToFirst();
                while (!cursor.isAfterLast()){
                    Task task = new Task(cursor.getInt(cursor.getColumnIndex(TaskTable.ID)),
                            cursor.getString(cursor.getColumnIndex(TaskTable.DATE)),
                            cursor.getString(cursor.getColumnIndex(TaskTable.DESCRIPTION)));
                    arrayList.add(task);
                    cursor.moveToNext();
                }

            }
        } finally {
            if(cursor != null)
                cursor.close();
        }

        return arrayList;
    }

}
