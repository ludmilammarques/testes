package ludprojetos.app.treino.database.model;

import java.io.Serializable;

public class Task implements Serializable {
    private int id;
    private String date;
    private String description;

    public Task() {
    }

    public Task(int id, String date, String description) {
        this.id = id;
        this.date = date;
        this.description = description;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
