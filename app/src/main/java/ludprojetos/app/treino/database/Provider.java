package ludprojetos.app.treino.database;

import android.content.ContentProvider;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.OperationApplicationException;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteQueryBuilder;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.ArrayList;

import ludprojetos.app.treino.database.tables.TaskTable;

public class Provider extends ContentProvider {
    //SQLiteOpenHelper
    private DbHelper database;
    private SQLiteDatabase batchOperationReference = null;

    private static final UriMatcher URI_MATCHER;
    private static final String AUTHORITY = "ludprojetos.app.treino.provider";
    private static final Uri AUTHORITY_URI = Uri.parse("content://" + AUTHORITY);
    private static final String CONTENT_PATH = "task";

    //TABLES URI
    public static final Uri TASK_URI = Uri.withAppendedPath(AUTHORITY_URI, CONTENT_PATH);

    private static final int TASK_DIR = 0;

    static {
        URI_MATCHER = new UriMatcher(UriMatcher.NO_MATCH);
        URI_MATCHER.addURI(AUTHORITY, CONTENT_PATH, TASK_DIR);
    }

    @Override
    public boolean onCreate() {
        database = new DbHelper(getContext());
        return true;
    }

    @Nullable
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs, String sortOrder) {

        final SQLiteQueryBuilder queryBuilder = new SQLiteQueryBuilder();
        final SQLiteDatabase dbConnection = database.getReadableDatabase();

        switch (URI_MATCHER.match(uri)){
            case TASK_DIR:
                queryBuilder.setTables(TaskTable.TABLE_NAME);
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI:" + uri);
        }

        Cursor cursor = queryBuilder.query(dbConnection, projection, selection, selectionArgs, null, null, sortOrder);
        cursor.setNotificationUri(getContext().getContentResolver(), uri);

        return cursor;
    }

    @Nullable
    @Override
    public String getType(Uri uri) {
        switch (URI_MATCHER.match(uri)){
            case TASK_DIR:
                return "vnd.android.cursor.dir/vnd.mdsdacp.task";
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }
    }

    @Nullable
    @Override
    public Uri insert(Uri uri, ContentValues values) {

        SQLiteDatabase dbConnection = null;

        try {
            if(batchOperationReference == null){
                //Se nenhum batch em operação, abra um conexão com o banco
                dbConnection = database.getWritableDatabase();
                dbConnection.beginTransaction();
            } else {
                dbConnection = batchOperationReference;
            }

            switch (URI_MATCHER.match(uri)){
                case TASK_DIR:
                    final long taskId = dbConnection.insertWithOnConflict(TaskTable.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE);
                    final Uri newTask = ContentUris.withAppendedId(TASK_URI, taskId);
                    getContext().getContentResolver().notifyChange(newTask, null);
                    if(batchOperationReference == null){
                        dbConnection.setTransactionSuccessful();
                    }
                    return newTask;
                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } catch (Exception e) {
            Log.e("PROVIDER", "Insert Exception", e);
        } finally {
            if (dbConnection != null && batchOperationReference == null) {
                // Do not close transaction if there is a batch operation in progress.
                // It will be done by applyBatch method
                dbConnection.endTransaction();
            }
        }
        return null;
    }

    @Override
    public int bulkInsert(Uri uri, ContentValues[] values) {
        String table = uri.getLastPathSegment();
        SQLiteDatabase dbConnection = database.getWritableDatabase();
        int res = 0;
        dbConnection.beginTransaction();
        try {
            for (ContentValues v : values) {
                long id = dbConnection.insertWithOnConflict(table, null, v, SQLiteDatabase.CONFLICT_REPLACE);
                dbConnection.yieldIfContendedSafely();
                if (id != -1) {
                    res++;
                }
            }
            dbConnection.setTransactionSuccessful();

        } finally {
            dbConnection.endTransaction();
        }
        if (res != 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return res;
    }

    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        final SQLiteDatabase dbConnection = database.getWritableDatabase();
        int deleteCount = 0;

        try{
            dbConnection.beginTransaction();
            switch (URI_MATCHER.match(uri)){
                case TASK_DIR:
                    deleteCount = dbConnection.delete(TaskTable.TABLE_NAME, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }

        if (deleteCount > 0) {
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return deleteCount;
    }

    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        final SQLiteDatabase dbConnection = database.getWritableDatabase();
        int updateCount = 0;

        try {
            dbConnection.beginTransaction();
            switch (URI_MATCHER.match(uri)){
                case TASK_DIR:
                    updateCount = dbConnection.update(TaskTable.TABLE_NAME, values, selection, selectionArgs);
                    dbConnection.setTransactionSuccessful();
                    break;
                default:
                    throw new IllegalArgumentException("Unsupported URI:" + uri);
            }
        } finally {
            dbConnection.endTransaction();
        }

        if(updateCount < 0){
            getContext().getContentResolver().notifyChange(uri, null);
        }

        return updateCount;
    }

    @Override
    public ContentProviderResult[] applyBatch(ArrayList< ContentProviderOperation > operations) {
        ContentProviderResult[] result = new ContentProviderResult[operations.size()];
        int i = 0;
        // Opens the database object in "write" mode.
        batchOperationReference = database.getWritableDatabase();

        // Begin a transaction
        batchOperationReference.beginTransaction();
        try {
            for (ContentProviderOperation operation : operations) {
                // Chain the result for back references
                result[i++] = operation.apply(this, result, i);
            }

            batchOperationReference.setTransactionSuccessful();
        } catch (OperationApplicationException e) {
            Log.e("PROVIDER", "batch failed: " + e.getLocalizedMessage());
        } finally {
            batchOperationReference.endTransaction();
            batchOperationReference = null;
        }

        return result;
    }
}
