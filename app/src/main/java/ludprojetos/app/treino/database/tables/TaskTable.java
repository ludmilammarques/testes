package ludprojetos.app.treino.database.tables;

public interface TaskTable {

    String TABLE_NAME = "task";

    String ID = "id";
    String DATE = "date";
    String DESCRIPTION = "description";

    String SQL_CREATE = "CREATE TABLE " + TABLE_NAME + "(" +
            ID + " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, " +
            DATE + " VARCHAR(20), " +
            DESCRIPTION + " VARCHAR(200) NOT NULL );";

    String SQL_DROP = "DROP TABLE IF EXISTS" + TABLE_NAME;
}
